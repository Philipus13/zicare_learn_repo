import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zi_care_mobile_challenge/exception/unexpected_error_exception.dart';
import 'package:zi_care_mobile_challenge/model/user_model.dart';
import 'package:zi_care_mobile_challenge/page/home/service/home_service.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial());

  final HomeService _service = HomeService();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is GetHome) {
      yield HomeLoading();
      try {
        yield await _service
            .getUser()
            .then((value) => HomeLoaded(userModel: value));
      } on UnexpectedErrorException catch (e) {
        yield HomeError(errorMessage: e.errorMessage);
      }
    }

    if (event is LogOutEvent) {
      SharedPreferences _instance = await SharedPreferences.getInstance();
      _instance.remove('authToken');
      yield LogoutSuccess();
    }
  }
}
