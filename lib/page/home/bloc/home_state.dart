part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoading extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoaded extends HomeState {
  final UserModel userModel;

  HomeLoaded({this.userModel});

  @override
  List<Object> get props => [this.userModel];
}

class HomeError extends HomeState {
  final String errorMessage;

  HomeError({this.errorMessage});

  @override
  List<Object> get props => [this.errorMessage];
}

class LogoutSuccess extends HomeState {
  @override
  List<Object> get props => [];
}
