part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class GetHome extends HomeEvent {
  @override
  List<Object> get props => [];
}

class LogOutEvent extends HomeEvent {
  @override
  List<Object> get props => [];
}
