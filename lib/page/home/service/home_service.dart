import 'package:zi_care_mobile_challenge/exception/unexpected_error_exception.dart';
import 'package:zi_care_mobile_challenge/model/user_model.dart';
import 'package:zi_care_mobile_challenge/network/base_network.dart';

class HomeService extends BaseNetworking {
  Future<UserModel> getUser() async {
    final String path = 'api/v1/users/me/';
    return service.getAuthToken(additionalPath: path).then(
          (value) => value.success
              ? UserModel.fromJson(value.data['user'])
              : throw UnexpectedErrorException(errorMessage: value.message),
        );
  }
}
