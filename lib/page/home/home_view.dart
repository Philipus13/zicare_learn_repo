import 'package:flutter/material.dart';
import 'package:zi_care_mobile_challenge/model/user_model.dart';
import 'package:zi_care_mobile_challenge/page/style/base_style.dart';

class HomeView extends StatelessWidget {
  final UserModel userModel;
  final Function onLogout;
  final bool isLoading;

  const HomeView({Key key, this.userModel, this.onLogout, this.isLoading})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        centerTitle: true,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: isLoading || userModel == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Username',
                            style: BaseStyle.ts14ExplicitBlackBold,
                          ),
                          SizedBox(height: 10),
                          Text('${userModel.username}'),
                          SizedBox(height: 15),
                          Text(
                            'Full Name',
                            style: BaseStyle.ts14ExplicitBlackBold,
                          ),
                          SizedBox(height: 10),
                          Text('${userModel.fullName}'),
                          SizedBox(height: 15),
                          Text(
                            'Email',
                            style: BaseStyle.ts14ExplicitBlackBold,
                          ),
                          SizedBox(height: 10),
                          Text('${userModel.email}'),
                          SizedBox(height: 15),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.all(16),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        disabledColor: BaseStyle.disableBtnColor,
                        color: BaseStyle.orangeBtnColor,
                        onPressed: () {
                          onLogout();
                        },
                        child: Text(
                          'Log Out',
                          style: BaseStyle.ts14WhiteBold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
