import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zi_care_mobile_challenge/page/home/bloc/home_bloc.dart';
import 'package:zi_care_mobile_challenge/page/home/home_view.dart';
import 'package:zi_care_mobile_challenge/page/sign_in/sigin_in_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = HomeBloc();
    _bloc.add(GetHome());
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      listener: (BuildContext context, HomeState state) {
        if (state is LogoutSuccess) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => SignInPage()),
              (route) => false);
        }
      },
      child: BlocBuilder(
        cubit: _bloc,
        builder: (context, HomeState state) {
          return HomeView(
            userModel: (state is HomeLoaded) ? state.userModel : null,
            onLogout: onLogout,
            isLoading: (state is HomeLoading),
          );
        },
      ),
    );
  }

  onLogout() {
    _bloc.add(LogOutEvent());
  }
}
