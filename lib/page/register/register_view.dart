import 'package:flutter/material.dart';
import 'package:zi_care_mobile_challenge/page/style/base_style.dart';

class RegisterView extends StatelessWidget {
  final RegExp emailRegExp = RegExp(r'@');
  final RegExp passRegexp = RegExp(r"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,12}$");
  final bool autoval;
  final GlobalKey<FormState> formKey;
  final String email;
  final String password;
  final bool obsecurePass;
  final bool buttonEnable;
  final TextEditingController emailController;
  final TextEditingController fullnameController;
  final TextEditingController usernameController;
  final Function(String) onChangeEmail;
  final Function(String) onChangePassword;
  final Function(String) onChangeFullname;
  final Function(String) onChangeUsername;
  final Function onRegister;
  final Function onTapShowed;
  final Function onTapExit;
  final bool isLoading;
  final String errorMessage;

  RegisterView({
    Key key,
    this.autoval,
    this.formKey,
    this.email,
    this.password,
    this.obsecurePass,
    this.buttonEnable,
    this.emailController,
    this.onChangeEmail,
    this.onChangePassword,
    this.onRegister,
    this.onTapShowed,
    this.onTapExit,
    this.isLoading,
    this.fullnameController,
    this.usernameController,
    this.onChangeFullname,
    this.onChangeUsername,
    this.errorMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 24),
                child: InkWell(
                  onTap: () {
                    onTapExit();
                  },
                  child: Icon(
                    Icons.close,
                    size: 24,
                    color: Colors.grey[500],
                  ),
                ),
              ),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.fromLTRB(16, 10, 16, 0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Register',
                    style: BaseStyle.ts18DarkBlueBold,
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    'Register an account, to sign in into the application',
                    style: BaseStyle.ts14PrimaryBlack,
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Divider(),
                  SizedBox(
                    height: 24,
                  ),
                  Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Full Name',
                          style: BaseStyle.ts14ExplicitBlackBold,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          controller: fullnameController,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(16),
                              fillColor: BaseStyle.hintBgTextfieldColor,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6),
                                borderSide:
                                    BorderSide(color: BaseStyle.darkBlue),
                              ),
                              hintText: 'Example : Philipus Silaen'),
                          keyboardType: TextInputType.emailAddress,
                          style: BaseStyle.ts14ExplicitBlack,
                          validator: (String textin) {
                            if (textin.isEmpty) {
                              return "Full Name can not be empty";
                            } else if (textin.length <= 2) {
                              return "Full Name must longer than 2 characters";
                            }
                            return null;
                          },
                          autovalidate: autoval,
                          onChanged: onChangeFullname,
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Text(
                          'Username',
                          style: BaseStyle.ts14ExplicitBlackBold,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          controller: usernameController,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(16),
                              fillColor: BaseStyle.hintBgTextfieldColor,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6),
                                borderSide:
                                    BorderSide(color: BaseStyle.darkBlue),
                              ),
                              hintText: 'Example : philipus123'),
                          keyboardType: TextInputType.text,
                          style: BaseStyle.ts14ExplicitBlack,
                          validator: (String textin) {
                            if (textin.isEmpty) {
                              return "Username can not be empty";
                            } else if (textin.length < 6) {
                              return "Username must longer than 5 characters";
                            }
                            return null;
                          },
                          autovalidate: autoval,
                          onChanged: onChangeUsername,
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Text(
                          'Email',
                          style: BaseStyle.ts14ExplicitBlackBold,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          controller: emailController,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(16),
                              fillColor: BaseStyle.hintBgTextfieldColor,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6),
                                borderSide:
                                    BorderSide(color: BaseStyle.darkBlue),
                              ),
                              hintText: 'Example : yourname@mail.com'),
                          keyboardType: TextInputType.emailAddress,
                          style: BaseStyle.ts14ExplicitBlack,
                          validator: (String textin) {
                            if (textin.isEmpty) {
                              return "Email can not be empty";
                            } else if (!emailRegExp
                                .hasMatch(textin.toString())) {
                              return "Please insert valid email";
                            }
                            return null;
                          },
                          autovalidate: autoval,
                          onChanged: onChangeEmail,
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Text(
                          'Password',
                          style: BaseStyle.ts14ExplicitBlackBold,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(16),
                              fillColor: BaseStyle.hintBgTextfieldColor,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6),
                                borderSide:
                                    BorderSide(color: BaseStyle.darkBlue),
                              ),
                              hintText: 'Type your password here',
                              suffixIcon: FlatButton(
                                onPressed: () {
                                  onTapShowed();
                                },
                                child: Text(
                                  obsecurePass ? 'Show' : 'Hide',
                                  style: BaseStyle.ts14ExplicitBlackBold,
                                ),
                              )),
                          obscureText: obsecurePass,
                          keyboardType: TextInputType.visiblePassword,
                          style: BaseStyle.ts14ExplicitBlack,
                          validator: (String textin) {
                            if (textin.isEmpty) {
                              return "Password can not be empty";
                            } else if (!passRegexp
                                .hasMatch(textin.toString())) {
                              return "Password must be contain number, character, and sign";
                            }
                            return null;
                          },
                          autovalidate: true,
                          onChanged: onChangePassword,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 83,
                  ),
                  // loginSuccess
                  //     ? Align(
                  //         alignment: Alignment.center,
                  //         child: Text(''),
                  //       )
                  //     : Align(
                  //         alignment: Alignment.center,
                  //         child: Text(
                  //           'Your email or password is incorrect',
                  //           style: BaseStyle.ts14RedBold,
                  //         ),
                  //       ),
                  errorMessage.isNotEmpty
                      ? Text(
                          '$errorMessage',
                          style: BaseStyle.ts12RedBold,
                        )
                      : Container(),
                  SizedBox(
                    height: 16,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      padding: EdgeInsets.all(16),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      disabledColor: BaseStyle.disableBtnColor,
                      color: BaseStyle.orangeBtnColor,
                      onPressed: !emailRegExp.hasMatch(email) ||
                              !passRegexp.hasMatch(password) ||
                              isLoading
                          ? null
                          : () {
                              onRegister();
                            },
                      child: isLoading
                          ? CircularProgressIndicator()
                          : Text(
                              'Register',
                              style: BaseStyle.ts14WhiteBold,
                            ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
