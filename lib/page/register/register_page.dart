import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zi_care_mobile_challenge/page/register/bloc/register_bloc.dart';
import 'package:zi_care_mobile_challenge/page/register/register_view.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool autoval = false;
  GlobalKey<FormState> globalKey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  String username = '';
  String fullname = '';
  bool obsecurePass = true;
  bool buttonEnable = false;
  var emailController = TextEditingController();
  var fullnameController = TextEditingController();
  var usernameController = TextEditingController();
  RegisterBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = RegisterBloc();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      child: BlocBuilder(
        cubit: _bloc,
        builder: (BuildContext context, RegisterState state) {
          return RegisterView(
            autoval: autoval,
            buttonEnable: buttonEnable,
            email: email,
            emailController: emailController,
            formKey: globalKey,
            obsecurePass: obsecurePass,
            onChangeEmail: onChangeEmail,
            onChangePassword: onChangePassword,
            onRegister: _registerButtonPressed,
            onTapShowed: onTapShowed,
            password: password,
            onTapExit: onTapExit,
            isLoading: (state is RegisterLoading),
            fullnameController: fullnameController,
            onChangeFullname: onChangeFullname,
            onChangeUsername: onChangeUsername,
            usernameController: usernameController,
            errorMessage: (state is RegisterFailed) ? state.errorMessage : '',
          );
        },
      ),
      listener: (BuildContext context, RegisterState state) {
        if (state is RegisterSuccess) {
          showDialog(
            context: context,
            builder: (_) =>
            Dialog(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Text('Registrasi Berhasil'),
              ),
            ),
          );
        }
      },
    );
  }

  onTapExit() {
    Navigator.pop(context);
  }

  onChangeEmail(String value) {
    setState(() {
      email = value;
    });
  }

  onChangeUsername(String value) {
    setState(() {
      username = value;
    });
  }

  onChangeFullname(String value) {
    setState(() {
      fullname = value;
    });
  }

  onChangePassword(String value) {
    setState(() {
      password = value;
    });
  }

  onTapShowed() {
    setState(() {
      obsecurePass = !obsecurePass;
    });
  }

  void _registerButtonPressed() {
    if (!globalKey.currentState.validate()) {
      setState(() {
        autoval = true;
      });
    } else {
      _bloc.add(RegisterUser(
          email: email,
          fullName: fullname,
          password: password,
          username: username));
    }
  }
}
