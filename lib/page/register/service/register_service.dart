import 'package:zi_care_mobile_challenge/exception/unexpected_error_exception.dart';
import 'package:zi_care_mobile_challenge/model/user_model.dart';
import 'package:zi_care_mobile_challenge/network/base_network.dart';

class RegisterService extends BaseNetworking {
  final String _path = 'api/v1/auth/register';

  Future<bool> registerUser(Map<String, String> data) async {
    return await service
        .postNoAuthToken(
          data: data,
          path: _path,
        )
        .then((value) => value.success
            ? value.success
            : throw UnexpectedErrorException(errorMessage: value.message));
  }
}
