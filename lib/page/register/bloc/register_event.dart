part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterUser extends RegisterEvent {
  final String username;
  final String password;
  final String email;
  final String fullName;

  RegisterUser({this.username, this.password, this.email, this.fullName});

  @override
  List<Object> get props =>
      [this.username, this.password, this.email, this.fullName];
}
