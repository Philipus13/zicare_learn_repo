import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:zi_care_mobile_challenge/exception/unexpected_error_exception.dart';
import 'package:zi_care_mobile_challenge/model/user_model.dart';
import 'package:zi_care_mobile_challenge/page/register/service/register_service.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc() : super(RegisterInitial());

  final RegisterService _registerService = RegisterService();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterUser) {
      yield RegisterLoading();
      try {
        final data = <String, String>{
          'full_name': event.fullName,
          'username': event.username,
          'email': event.email,
          'password': event.password
        };
        yield await _registerService
            .registerUser(data)
            .then((value) => RegisterSuccess());
      } on UnexpectedErrorException catch (e) {
        yield RegisterFailed(errorMessage: e.errorMessage);
      }
    }
  }
}
