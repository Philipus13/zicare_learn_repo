part of 'sign_in_bloc.dart';

abstract class SignInState extends Equatable {
  const SignInState();
}

class SignInInitial extends SignInState {
  @override
  List<Object> get props => [];
}

class SignInSuccess extends SignInState {
  final String token;

  SignInSuccess({this.token});
  @override
  List<Object> get props => [this.token];
}

class SignInError extends SignInState {
  final String errMessage;

  SignInError({this.errMessage});

  @override
  List<Object> get props => [this.errMessage];
}

class SaveTokenSuccess extends SignInState {
  @override
  List<Object> get props => null;
}

class SaveTokenError extends SignInState {
  @override
  List<Object> get props => null;
}

class SignInLoading extends SignInState {
  @override
  List<Object> get props => null;
}
