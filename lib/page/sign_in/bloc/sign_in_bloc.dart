import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zi_care_mobile_challenge/exception/unexpected_error_exception.dart';
import 'package:zi_care_mobile_challenge/page/sign_in/service/sign_in_service.dart';

part 'sign_in_event.dart';
part 'sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc() : super(SignInInitial());

  SignInService _service = SignInService();
  SharedPreferences _preferences;

  @override
  Stream<SignInState> mapEventToState(
    SignInEvent event,
  ) async* {
    if (event is SignInAccountEvent) {
      yield SignInLoading();
      try {
        yield await _service
            .signInAccount(username: event.username, password: event.password)
            .then((token) {
          return SignInSuccess(token: token);
        });
      } on UnexpectedErrorException catch (e) {
        yield SignInError(errMessage: e.errorMessage);
      }
    }

    if (event is SaveTokenToLocalEvent) {
      yield SignInLoading();
      _preferences = await SharedPreferences.getInstance();
      try {
        yield await _preferences
            .setString('authToken', event.token)
            .then((value) {
          if (value) {
            return SaveTokenSuccess();
          }
          return SaveTokenError();
        });
      } catch (_) {
        yield SaveTokenError();
      }
    }
  }
}
