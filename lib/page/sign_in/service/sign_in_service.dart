import 'package:zi_care_mobile_challenge/network/base_network.dart';

class SignInService extends BaseNetworking {
  Future<String> signInAccount({String username, String password}) async {
    return await service.postGetToken(username: username, password: password);
  }
}
