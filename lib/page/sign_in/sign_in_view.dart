import 'package:flutter/material.dart';
import 'package:zi_care_mobile_challenge/page/style/base_style.dart';

class SignInView extends StatelessWidget {
  final RegExp passRegexp = RegExp(r"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,12}$");
  final bool autoval;
  final GlobalKey<FormState> formKey;
  final String username;
  final String password;
  final bool obsecurePass;
  final bool buttonEnable;
  final TextEditingController usernameController;
  final Function(String) onChangeUsername;
  final Function(String) onChangePassword;
  final Function onLogin;
  final Function onTapShowed;
  final Function onTapExit;
  final Function toRegisterPage;
  final bool isLoading;

  SignInView({
    Key key,
    this.autoval,
    this.formKey,
    this.username,
    this.password,
    this.obsecurePass,
    this.buttonEnable,
    this.usernameController,
    this.onChangeUsername,
    this.onChangePassword,
    this.onLogin,
    this.onTapShowed,
    this.onTapExit,
    this.isLoading,
    this.toRegisterPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 24),
                child: InkWell(
                  onTap: () {
                    onTapExit();
                  },
                  child: Icon(
                    Icons.close,
                    size: 24,
                  ),
                ),
              ),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.fromLTRB(16, 10, 16, 0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Sign In',
                    style: BaseStyle.ts18DarkBlueBold,
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    'Use your created account to sign in into the application',
                    style: BaseStyle.ts14PrimaryBlack,
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Divider(),
                  SizedBox(
                    height: 24,
                  ),
                  Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Username',
                          style: BaseStyle.ts14ExplicitBlackBold,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          controller: usernameController,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(16),
                              fillColor: BaseStyle.hintBgTextfieldColor,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6),
                                borderSide:
                                    BorderSide(color: BaseStyle.darkBlue),
                              ),
                              hintText: 'Example : philipus13'),
                          keyboardType: TextInputType.text,
                          style: BaseStyle.ts14ExplicitBlack,
                          validator: (String textin) {
                            if (textin.isEmpty) {
                              return "Username can not be empty";
                            } else if (textin.length <= 6) {
                              return "Username must be longer than 6 characters";
                            }
                            return null;
                          },
                          autovalidate: autoval,
                          onChanged: onChangeUsername,
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Text(
                          'Password',
                          style: BaseStyle.ts14ExplicitBlackBold,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(16),
                              fillColor: BaseStyle.hintBgTextfieldColor,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(6),
                                borderSide:
                                    BorderSide(color: BaseStyle.darkBlue),
                              ),
                              hintText: 'Type your password here',
                              suffixIcon: FlatButton(
                                onPressed: () {
                                  onTapShowed();
                                },
                                child: Text(
                                  obsecurePass ? 'Show' : 'Hide',
                                  style: BaseStyle.ts14ExplicitBlackBold,
                                ),
                              )),
                          obscureText: obsecurePass,
                          keyboardType: TextInputType.visiblePassword,
                          style: BaseStyle.ts14ExplicitBlack,
                          validator: (String textin) {
                            if (textin.isEmpty) {
                              return "Password can not be empty";
                            } else if (!passRegexp
                                .hasMatch(textin.toString())) {
                              return "Password must be contain number, character, and sign";
                            }
                            return null;
                          },
                          autovalidate: autoval,
                          onChanged: onChangePassword,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 83,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      padding: EdgeInsets.all(16),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      disabledColor: BaseStyle.disableBtnColor,
                      color: BaseStyle.orangeBtnColor,
                      onPressed: !passRegexp.hasMatch(password) || isLoading
                          ? null
                          : () {
                              onLogin();
                            },
                      child: isLoading
                          ? CircularProgressIndicator()
                          : Text(
                              'Sign In',
                              style: BaseStyle.ts14WhiteBold,
                            ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Dont have an account?',
                        style: BaseStyle.ts14PrimaryBlack,
                      ),
                      InkWell(
                        onTap: () {
                          toRegisterPage();
                        },
                        child: Text(
                          'Click here',
                          style: BaseStyle.ts14DarkBlue,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
