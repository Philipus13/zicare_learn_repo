import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:zi_care_mobile_challenge/page/home/home_page.dart';
import 'package:zi_care_mobile_challenge/page/register/register_page.dart';
import 'package:zi_care_mobile_challenge/page/sign_in/bloc/sign_in_bloc.dart';
import 'package:zi_care_mobile_challenge/page/sign_in/sign_in_view.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  bool autoval = false;
  GlobalKey<FormState> globalKey = GlobalKey<FormState>();
  String username = '';
  String password = '';
  bool obsecurePass = true;
  bool buttonEnable = false;
  var usernameController = TextEditingController();
  var loginSuccess = true;
  SignInBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = SignInBloc();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: _bloc,
      child: BlocBuilder(
        cubit: _bloc,
        builder: (BuildContext context, SignInState state) {
          return SignInView(
            autoval: autoval,
            buttonEnable: buttonEnable,
            username: username,
            usernameController: usernameController,
            formKey: globalKey,
            obsecurePass: obsecurePass,
            onChangeUsername: onChangeUsername,
            onChangePassword: onChangePassword,
            onLogin: _signInButtonPressed,
            onTapShowed: onTapShowed,
            password: password,
            toRegisterPage: toRegisterPage,
            onTapExit: onTapExit,
            isLoading: (state is SignInLoading),
          );
        },
      ),
      listener: (BuildContext context, SignInState state) {
        if (state is SaveTokenSuccess) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => HomePage()),
              (route) => false);
        }
        if (state is SignInSuccess) {
          _bloc.add(SaveTokenToLocalEvent(token: state.token));
        }
        if(state is SignInError){
           showDialog(
            context: context,
            builder: (_) =>
            Dialog(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Text(state.errMessage),
              ),
            ),
          );
        }
      },
    );
  }

  toRegisterPage() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => RegisterPage()));
  }

  onTapExit() {
    Navigator.pop(context);
  }

  onChangeUsername(String value) {
    setState(() {
      username = value;
    });
  }

  onChangePassword(String value) {
    setState(() {
      password = value;
    });
  }

  onTapShowed() {
    setState(() {
      obsecurePass = !obsecurePass;
    });
  }

  void _signInButtonPressed() {
    if (!globalKey.currentState.validate()) {
      setState(() {
        autoval = true;
      });
    } else {
      _bloc.add(SignInAccountEvent(username: username, password: password));
    }
  }
}
