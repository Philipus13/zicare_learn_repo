import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zi_care_mobile_challenge/page/home/home_page.dart';
import 'package:zi_care_mobile_challenge/page/sign_in/sigin_in_page.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
    checkToken();
  }

  void checkToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.getString('authToken') != null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => HomePage()),
          (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => SignInPage()),
          (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
