import 'package:shared_preferences/shared_preferences.dart';
import 'package:zi_care_mobile_challenge/exception/remote_data_source_exception.dart';
import 'package:zi_care_mobile_challenge/exception/unexpected_error_exception.dart';
import 'package:zi_care_mobile_challenge/network/network_model.dart';
import 'package:zi_care_mobile_challenge/network/network_utils.dart';

abstract class BaseNetworking {
  final BaseService _service = BaseService();

  BaseService get service => _service;
}

class BaseService {
  final String baseUrl = 'zpi-dev.zicare.id';

  BaseService();

  Future<Map<String, String>> _getHeaderWithAuth() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('authToken');
    var header = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
    return header;
  }

  Map<String, dynamic> getHeaderNoAuth() {
    var header = {'Content-Type': 'application/json'};
    return header;
  }

  Future<NetworkModel> postAuthToken({
    Map<String, dynamic> rawQuery,
    String path,
  }) async {
    try {
      final url = Uri.https(baseUrl, path);
      final body = <String, String>{
        // 'full_name': full_name,
        // 'username': username,
        // 'email': email,
        // 'password': password
      };
      final header = <String, String>{
        'Content-Type': 'application/json',
        'accept': 'application/json',
      };

      return await NetworkUtils.post(url, headers: header, body: body)
          .then((response) => NetworkModel.fromJson(response));
    } catch (err) {
      throw UnexpectedErrorException(errorMessage: err.response.message);
    }
  }

  Future<NetworkModel> getAuthToken({
    String additionalPath,
    Map<String, dynamic> queryParams,
  }) async {
    try {
      final url = Uri.https(baseUrl, additionalPath, queryParams);

      return await NetworkUtils.get(url, headers: await _getHeaderWithAuth())
          .then((response) => NetworkModel.fromJson(response));
    } catch (err) {
      throw UnexpectedErrorException(errorMessage: err.response.message);
    }
  }

  Future<NetworkModel> getNoAuthToken({
    String additionalPath,
    Map<String, dynamic> queryParams,
    String url,
  }) async {
    String baseUrl = url ?? this.baseUrl;
    String path = additionalPath ?? '';
    print('Method GET : $baseUrl$path \n Params: $queryParams');
    try {
      final url = Uri.https(baseUrl, path, queryParams);

      return await NetworkUtils.get(
        url,
      ).then((response) => NetworkModel.fromJson(response.data));
    } catch (err) {
      return NetworkModel.fromJson(err.response.data);
    }
  }

  Future<String> postGetToken ({String username, String password}) async {
    final String loginPath = 'api/v1/auth/token';
    try {
      final url = Uri.https(baseUrl, loginPath);
      final body = <String, String>{
        'username': username,
        'password': password,
        'grant_type': 'password',
      };

      return await NetworkUtils.post(url, body: body)
          .then((response) => response['access_token']);
    } catch (err) {
      if (err is RemoteDataSourceException) {
        throw UnexpectedErrorException(errorMessage: err.message);
      } else {
        throw UnexpectedErrorException(errorMessage: err.response.data);
      }
    }
  }

  Future<NetworkModel> postNoAuthToken({
    Map<String, String> data,
    String path,
  }) async {
    print('Method POST : ${this.baseUrl}$path \n Body: $data');
    try {
      final url = Uri.https(baseUrl, path);

      return await NetworkUtils.post(url,
              headers: await _getHeaderWithAuth(), body: data)
          .then((response) => NetworkModel.fromJson(response));
    } catch (err) {
      if (err is RemoteDataSourceException) {
        throw UnexpectedErrorException(errorMessage: err.message);
      } else {
        throw UnexpectedErrorException(errorMessage: err.response.data);
      }
      // return NetworkModel.fromJson(err.response.data);
    }
  }
}
