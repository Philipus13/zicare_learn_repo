import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:zi_care_mobile_challenge/app.dart';
import 'package:zi_care_mobile_challenge/bloc_delegate/simple_bloc_delegate.dart';
import 'package:zi_care_mobile_challenge/page/style/base_style.dart';

void main() {
  Bloc.observer = SimpleBlocDelegate();
  runApp(MainApplication());
}

class MainApplication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: BaseStyle.darkBlue,
        primaryColorLight: BaseStyle.lightBlue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => App(),
      },
    );
  }
}
