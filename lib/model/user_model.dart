class UserModel {
  String fullName;
  String username;
  String password;
  String email;

  UserModel({this.email, this.fullName, this.password, this.username});

  UserModel.fromJson(Map<String, dynamic> jsonResponse) {
    fullName = jsonResponse['full_name'] ?? '';
    username = jsonResponse['username'] ?? '';
    email = jsonResponse['email'] ?? '';
    password = jsonResponse['password'] ?? '';
  }

  static Map<String, dynamic> toJson(UserModel userModel) {
    return {
      'full_name': userModel.fullName,
      'username': userModel.username,
      'email': userModel.email,
      'password': userModel.password,
    };
  }
}
